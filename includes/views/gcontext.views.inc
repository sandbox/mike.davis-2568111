<?php
/**
 * @file
 * Provides support for the Views module.
 */

/**
 * Implements hook_views_plugins().
 */
function gcontext_views_plugins() {
  return array(
    'access' => array(
      'gcontext' => array(
        'title' => t('Group permission'),
        'handler' => 'gcontext_plugin_access_group_perm',
        'uses options' => TRUE,
      ),
    ),
  );
}